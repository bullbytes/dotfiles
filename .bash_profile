echo ".bash_profile runs"

### Default programs and system settings ###
############################################

export VISUAL=nvim
export EDITOR=nvim

export BROWSER=firefox
# Name of the terminal emulator executable
export TERMINAL=kitty

export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CACHE_HOME="$HOME/.local/cache"
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_PICTURES_DIR="$HOME/images"

export INPUTRC="$XDG_CONFIG_HOME/.inputrc"
# No history file for less
export LESSHISTFILE=/dev/null
export PASSWORD_STORE_DIR="$HOME/secrets/.password-store"

# See https://stackoverflow.com/questions/7109667/change-default-location-of-vimrc
export MYVIMRC="$XDG_CONFIG_HOME/nvim/init.vim"

# Needed to make Java programs like IntelliJ work: https://github.com/swaywm/sway/issues/595
export _JAVA_AWT_WM_NONREPARENTING=1

# Read man pages with an editor
export MANPAGER='nvim +Man!'

# -l loads the math library of calculator bc.
# -q doesn't show the GNU bc welcome text
export BC_ENV_ARGS="-lq"

# Extra LaTeX packages go in here. "texmf" stands for "TEX and METAFONT".
texMfHome="$XDG_DATA_HOME/texmf"
mkdir -p $texMfHome
export TEXMFHOME=$texMfHome

export GRIM_DEFAULT_DIR="$XDG_PICTURES_DIR/screenshots"

# Share an SSH authentication agent, to avoid having to add passwords for each session
# From here: https://unix.stackexchange.com/questions/90853/how-can-i-run-ssh-add-automatically-without-a-password-prompt#217223
# We start the agent if there's no socket yet
fixed_ssh_auth_sock=~/.ssh/ssh_auth_sock

if [ ! -S "$fixed_ssh_auth_sock" ]; then
    # Start the agent, eval will set the environment variables SSH_AUTH_SOCK and SSH_AGENT_PID
    eval $(ssh-agent)
    mkdir ~/.ssh 2> /dev/null
    ln -sf "$SSH_AUTH_SOCK" "$fixed_ssh_auth_sock"
fi

export SSH_AUTH_SOCK="$fixed_ssh_auth_sock"

# If we are in an interactive shell, also run bashrc. Thus, after we log in and get to the Linux console, Bash has the same settings as in the terminal emulator
# See https://unix.stackexchange.com/questions/26676/how-to-check-if-a-shell-is-login-interactive-batch
[[ $- == *i* ]] && source ~/.bashrc
