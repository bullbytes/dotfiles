# See https://sw.kovidgoyal.net/kitty/conf/

# Inspiration:
# https://github.com/keithlo/dots/blob/c9274ea206b1c8174431770e77e3e3306421a2f0/dot/kitty/kitty.conf
# https://github.com/kalmiz/dot-config/blob/c395f078d273422990b56f662d37973fda6cb655/kitty/kitty.conf
# https://github.com/echuraev/dotfiles/blob/eda8423a9da7d5f3efe6a78ec115235cf2255423/.config/kitty/kitty.conf
# https://github.com/brainmorsel/dotfiles/blob/977dfdaf7d5c4f38f1564b6e4c221f2353ba0a53/kitty/kitty.conf
# https://github.com/araspik/dotfiles/blob/321eff5cc587a73bf780e6f080a5a8cf87c32e37/kitty/kitty.conf
# https://github.com/jaketrent/dotfiles/blob/3c3fd72619d8617e83b90e07ff80d58f4d4c7896/bash/kitty.conf
# https://github.com/varchar-42/dotfiles/blob/a1915df1ad5f3e18de5020699fb1480917a577ae/kitty/.config/kitty/kitty.conf
# https://github.com/guilhermegregio/dotfiles/blob/181cdc660e8c0de18e5a8bbd6d778e3024fe4213/config/kitty/kitty.conf
# https://github.com/boris-chernysh/dotfiles/blob/53f8c87706841498657bc9f9eb34bd6cc3b2123d/kitty/kitty.conf

kitty_mod ctrl+alt

# In Kitty version 0.24.1, the default shell integration makes the cursor a vertical line, not a box. Config files where we set the cursor shape: inputrc and bashrc
shell_integration disabled

# https://sw.kovidgoyal.net/kitty/conf/#opt-kitty.disable_ligatures
disable_ligatures always

cursor_blink_interval 0

# No beep
enable_audio_bell no

adjust_line_height 5

# Open the current kitty buffer in an editor. Overlay means the editor uses the current kitty window. Alternative type: os-window
map kitty_mod+i launch --stdin-source=@screen_scrollback --type=overlay nvim

# Open new kitty instance in a new window in the current working directory
map kitty_mod+enter new_os_window_with_cwd

# Fill new space with lines from the scrollback buffer after enlarging a window
scrollback_fill_enlarged_window yes

# Per default, "kitty_mod + e" is "open_url_with_hints"
# https://sw.kovidgoyal.net/kitty/conf/#shortcut-kitty.Open-URL
map kitty_mod+e kitten hints --hints-text-color green

# https://sw.kovidgoyal.net/kitty/conf/#font-sizes
# The default shortcut changed the font size of not only the current kitty window but also those that were spawned with `new_os_window`
map kitty_mod+equal change_font_size current +1.0
map kitty_mod+minus change_font_size current -1.0
map kitty_mod+backspace change_font_size current 0

# BEGIN_KITTY_THEME
# Light Custom
include current-theme.conf
# END_KITTY_THEME
