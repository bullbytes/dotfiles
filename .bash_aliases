alias e="$EDITOR"

# Define defaults for commands
alias cal='cal --monday'
alias ncdu='ncdu --color off'

# Ask before overwriting a file
alias mv='mv --interactive'

# Ask before overwriting a file. Copy directories recursively. Don't follow symlinks in the source file. Preserve all file attributes
alias cp='cp --interactive --archive'

# ls uses colors to, for example, distinguish files and folders
# To use customized colors for ls, set the environment variable LS_COLORS. Helps with the format: https://geoff.greer.fm/lscolors/
alias ls='ls --color=auto'

alias grep='grep --color=auto'
alias ssh='custom_ssh() { ssh "$@"; echo "SSH connection closed"; }; custom_ssh'

# Upgrading the kernel without restarting can lead to external hardware not being recognized anymore: https://superuser.com/questions/1188895/usb-detected-but-not-found-in-dev#1188912
# The parenthesis create a subshell meaning the verbose output caused by "set -x" is unset when the subshell exits at the end of the command. From https://stackoverflow.com/questions/12231792/echo-command-and-then-run-it-like-make
alias upgrade-with-ignore-list="(set -x; sudo pacman -Syu --ignore khal,python-urwid,linux,linux-firmware,linux-firmware-whence,linux-api-headers)"

# Remove orphaned packages.
# Needs single quotes, otherwise the newlines that "pacman -Qqdt" outputs
# cause trouble.
# The parenthesis create a subshell meaning the verbose output caused by "set -x" is unset when the subshell exits at the end of the command. From https://stackoverflow.com/questions/12231792/echo-command-and-then-run-it-like-make
# Arguments to "pacman -Q":
#    -d restrict output to packages installed as dependencies
#    -t list packages that are no longer required by any installed package
#    -q suppress version numbers of packages (this would confuse pacman -R)
alias cleanup_packages='(set -x; sudo pacman -Rs $(pacman -Qdtq))'

