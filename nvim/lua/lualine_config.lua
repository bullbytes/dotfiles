local lualine = require('lualine')

-- lualine.setup({ options = { theme = 'gruvbox' } })

local function get_lsp_status()
    local clients = vim.lsp.get_active_clients()
    if #clients == 0 then
        return ""  -- No output if no LSPs are active
    end
    local client_names = {}
    for _, client in ipairs(clients) do
        table.insert(client_names, client.name)
    end
    local prefix = #clients == 1 and "LSP: " or "LSPs: "
    return prefix .. table.concat(client_names, ", ")
end


require('lualine').setup {
    sections = {
        lualine_c = {
            { 'filename' },
            { get_lsp_status }  -- Use the named function here
        },
    },
}
