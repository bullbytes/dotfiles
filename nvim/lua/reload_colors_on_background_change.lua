local backgroundFile = vim.fn.expand('~/.config/nvim/colors/background.vim')
local function reload()
  -- print("Lua: Background color file was changed → Calling SetColors in init.vim")
  vim.fn.SetColors()
end

local w = vim.loop.new_fs_event()
local on_change
local function watch_file(fname)
  w:start(fname, {}, vim.schedule_wrap(on_change))
end
on_change = function()
  reload()
  -- Debounce: stop/start.
  w:stop()
  watch_file(backgroundFile)
end

watch_file(backgroundFile)
