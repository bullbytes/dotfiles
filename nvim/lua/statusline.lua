local M = {}


-- A table to store the LSP client names and their display names as a function. We might use different icons depending on the state of the client. For example the Copilot icon can be different if Copilot is enabled or disabled)
M.lsp_client_name_functions = {}

local function is_virtual_console()
  return vim.env.TERM == "linux"
end

M.is_virtual_console = is_virtual_console()
M.copilot_is_enabled = true

local function get_separator()
  if M.is_virtual_console then
    return " | "
  else return "｜"
  end
end

M.separator = get_separator()


local function transform_client_name(client)
  local display_client_name = client
  -- Assumes that Nerd Fonts are installed
  if client == "GitHub Copilot" then
    display_client_name = function ()
        return M.copilot_is_enabled and "" or ""
    end
    -- The Eclipse LSP client is called "eclipse.jdt.ls"
    elseif client == "jdtls" then
      display_client_name = function() return "" end
  end
  return display_client_name
end

local function add_client_to_list(client)
  if client then
      local client_name_function = function() return client.name end
      -- If $TERM is "linux" then don't change the client name, otherwise do
      if not M.is_virtual_console then
        client_name_function = transform_client_name(client.name)
      end

      M.lsp_client_name_functions[client.name] = client_name_function
    else
      print("LSP client is nil")
  end
end

-- Function to register LspAttach autocmd (only runs once)
local function setup_lsp_autocmd()
    vim.api.nvim_create_autocmd("LspAttach", {
        callback = function(args)
            local client = vim.lsp.get_client_by_id(args.data.client_id)
            add_client_to_list(client)
        end
    })
    vim.api.nvim_create_autocmd("LspDetach", {
      callback = function(args)
        local client = vim.lsp.get_client_by_id(args.data.client_id)
        if client then
          print("Detaching client: " .. client.name)
          M.lsp_client_name_functions[client.name] = nil
        end
      end,
    })

end

local function add_current_lsp_clients()
  local clients = vim.lsp.get_clients()
  for _, client in ipairs(clients) do
    add_client_to_list(client)
  end
end

local function is_empty(t)
  return t == nil or next(t) == nil
end

function M.get_lsp_status ()
  if is_empty(M.lsp_client_name_functions) then
      return M.separator .. "No LSPs"
  end
  local client_display_name_functions = vim.tbl_values(M.lsp_client_name_functions)

  local client_display_names = vim.tbl_map(function(fn) return fn() end, client_display_name_functions)

  local prefix = #client_display_names == 1 and "LSP: " or "LSPs: "
  return M.separator .. prefix .. table.concat(client_display_names, " ")

end

local function check_copilot_status()
  -- We could try changing the table to a string-to-string map and set the the value (the display name) for Copilot right here
  M.copilot_is_enabled = vim.fn["copilot#Enabled"]() == 1
  -- We previously used this:
  -- local status = vim.fn.execute("Copilot status")
  -- M.copilot_is_enabled = not string.find(status,"^\nCopilot: Disabled")
end

local function start_lsp_check_timer()
    -- Run every 10 seconds
    vim.fn.timer_start(10000, function()
        -- Since the attaching of GitHub Copilot apparently doesn't trigger LspAttach,
        -- we periodically check for attached LSP clients. Maybe other LSP clients don't trigger the event as well
        add_current_lsp_clients()
        -- In earlier versions, we called "Copilot status" each time the statusline gets updated (after each cursor move for example). This caused the pop up window shown when hovering with JDTLS to not close itself and also "Plug Update" to emit an error: "E565: Not allowed to change text or change window". This is why we check Copilot's status this way now
        check_copilot_status()
        -- Restart the timer for continuous checking
        start_lsp_check_timer()
    end)
end


add_current_lsp_clients()
setup_lsp_autocmd()
start_lsp_check_timer()

return M
