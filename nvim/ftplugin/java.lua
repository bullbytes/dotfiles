-- https://github.com/mfussenegger/nvim-jdtls?tab=readme-ov-file#user-content-configuration-quickstart
-- See also this: https://github.com/mfussenegger/nvim-jdtls/wiki/Sample-Configurations
local jdtls = require('jdtls')

-- Setup nvim-cmp
-- local cmp = require("cmp")
--
-- cmp.setup({
--   mapping = cmp.mapping.preset.insert({
--     ["<Tab>"] = cmp.mapping.select_next_item(),
--     ["<S-Tab>"] = cmp.mapping.select_prev_item(),
--     ["<CR>"] = cmp.mapping.confirm({ select = true }), -- Confirm completion
--       ['<C-b>'] = cmp.mapping.scroll_docs(-4),
--       ['<C-f>'] = cmp.mapping.scroll_docs(4),
--   }),
--    window = {
--         completion = cmp.config.window.bordered(),
--         documentation = cmp.config.window.bordered(),
--       },
--   sources = cmp.config.sources({
--     { name = "nvim_lsp" },  -- LSP-based autocompletion
--     { name = "buffer" },     -- Buffer words
--     { name = "path" },       -- File paths
--   }),
--   formatting = {
--     format = function(entry, vim_item)
--       -- Show JavaDocs in the completion popup (if available)
--       vim_item.menu = ({
--         nvim_lsp = "[LSP]",
--         buffer = "[Buffer]",
--         path = "[Path]",
--       })[entry.source.name]
--       return vim_item
--     end,
--   },
-- })
-- local cmp_lsp_capabilities = require('cmp_nvim_lsp').default_capabilities()

local config = {
  cmd = {'/usr/bin/jdtls'},
  root_dir = vim.fs.dirname(vim.fs.find({'gradlew', '.git', 'mvnw', "README.md"}, { upward = true })[1]),
  -- capabilities = cmp_lsp_capabilities,
  settings ={java={

    -- signatureHelp = {enabled = true}, contentProvider = {preferred = 'fernflower'},

    project={referencedLibraries={'~/.m2/repository/**/*.jar', '~/.jbang/cache/jars/**/*.jar'}}}},
  on_attach = function(client, bufnr)

    print("Attaching to jdtls")
    local wk = require("which-key")
    -- Add keymappings for this buffer only
    wk.add({
      { "<leader>do", jdtls.organize_imports, desc = "Organize imports", mode = "n", buffer = bufnr },
      { "<leader>dev", jdtls.extract_variable, desc = "Extract variable", mode = "n", buffer = bufnr },
      { "<leader>dev", function() jdtls.extract_variable(true) end, desc = "Extract variable", mode = "v", buffer = bufnr },
      { "<leader>dec", jdtls.extract_constant, desc = "Extract constant", mode = "n", buffer = bufnr },
      { "<leader>dem", jdtls.extract_method, desc = "Extract method", mode = "n", buffer = bufnr },
      { "<leader>dem", function() jdtls.extract_method(true) end, desc = "Extract method", mode = "v", buffer = bufnr },
    })
  end
}

jdtls.start_or_attach(config)


-- Show documentation when using coq nvim
vim.api.nvim_create_autocmd("LspAttach", {
  callback = function(args)
    local bufnr = args.buf
    local client = vim.lsp.get_client_by_id(args.data.client_id)
    if vim.tbl_contains({ 'null-ls' }, client.name) then  -- blacklist lsp
      return
    end
    require("lsp_signature").on_attach({
      -- ... setup options here ...
    }, bufnr)
  end,
})

-- "sout" expands to System.out.println(); and moves the cursor between the parentheses
vim.api.nvim_command('ia sout System.out.println();<Left><Left>')


-- Merge the sign column (gutter with, for example, warnings and error markers)
-- with the number column
vim.opt_local.signcolumn = "number"
vim.opt_local.number = true
