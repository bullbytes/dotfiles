" Mind that .vim files in $XDG_CONFIG_HOME/plugin are sourced as well
" Vim's root dir containing this config file. For example ~/.config/nvim
let g:vimDir = fnamemodify(expand("$MYVIMRC"), ':p:h')

""" Plugin manager
" vim-plug: https://github.com/junegunn/vim-plug

let vimPlugFile = g:vimDir . '/autoload/plug.vim'

" Install vim-plug if not present
if empty(glob(vimPlugFile))
    " There is no vim-plug file → Download and install it
    let vimPlugUrl = 'https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
    exec '!curl -fLo ' . vimPlugFile . ' --create-dirs ' . vimPlugUrl
    autocmd VimEnter * PlugInstall
endif

call plug#begin()

" Surrounds text with other text like parentheses or HTML tags
Plug 'https://github.com/tpope/vim-surround'
" Repeats the previous action from vim-surround
Plug 'https://github.com/tpope/vim-repeat'
" For Unix file operations like rename or delete
Plug 'https://github.com/tpope/vim-eunuch'
" Easier commenting for all kinds of source files
Plug 'https://github.com/tomtom/tcomment_vim'
" Integrates the fuzzy file finder fzf into Vim
" Alternatives:
" https://github.com/Yggdroot/LeaderF
" Telescope
Plug 'https://github.com/junegunn/fzf.vim'
Plug 'https://github.com/gioele/vim-autoswap'
" Lets me activate markers in the buffer and jump to them
" See :h easymotion for default key bindings
" To change colors, see :h easymotion-custom-hl
" Alternatives:
" https://github.com/ggandor/leap.nvim
" https://github.com/smoka7/hop.nvim
" https://github.com/folke/flash.nvim
Plug 'https://github.com/easymotion/vim-easymotion'
" Makes Vifm the file picker for Vim
Plug 'https://github.com/vifm/vifm.vim'
" Using gF, I can open a file and jump to a specific line using file path and
" line number, for example: ~/docs/lists/web_bookmarks/tech_bookmarks.txt:57
Plug 'https://github.com/wsdjeg/vim-fetch'
" Provides more text objects. Allows to change content inside angle brackets
" using ci< for example
Plug 'https://github.com/wellle/targets.vim'
" Easier to go back to earlier versions of the file
Plug 'https://github.com/mbbill/undotree'
" Preview Markdown in the browser (ran :call mkdp#util#install() to install it)
" Alternatives:
" * [Smeagol](https://smeagol.dev/)
" * [Gollum](https://github.com/gollum/gollum)
" * https://github.com/jannis-baum/vivify
Plug 'https://github.com/iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() }, 'for': ['markdown', 'vim-plug']}
" Automatically format (Markdown) tables
Plug 'https://github.com/dhruvasagar/vim-table-mode'
" Automatically save and restore a file's views which contain folds
Plug 'https://github.com/vim-scripts/restore_view.vim'
" Alternatives:
" https://github.com/kshenoy/vim-signature
Plug 'https://github.com/tpope/vim-fugitive'
" Shows colors as background for hex representations of colors. Do 'set termguicolors' for accuracy
" Alternatives: https://github.com/chrisbra/Colorizer
Plug 'https://github.com/norcalli/nvim-colorizer.lua'
Plug 'https://github.com/github/copilot.vim'
" Colors parentheses
Plug 'https://github.com/junegunn/rainbow_parentheses.vim'
" Also integrates the language server protocol (LSP) for Rust
Plug 'https://github.com/mrcjkb/rustaceanvim'
" Shows keymappings
Plug 'https://github.com/folke/which-key.nvim'
" Calculates the sum of visually selected numbers. In visual block mode:
" <leader><leader>?s
Plug 'https://github.com/sk1418/HowMuch'
" With <c-j> we go down one line but the cursor stays in the same relative
" position on the screen
Plug 'https://github.com/preservim/vim-wheel'

" Use ':TSInstall rust' to install the Rust parser, for example. TSInstallInfo
" shows the available and installed parsers
Plug 'https://github.com/nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
" Helper functions for telescope.nvim
Plug 'https://github.com/nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim', { 'tag': '0.1.x' }

" Java LSP support
" Java LSP server: https://github.com/eclipse-jdtls/eclipse.jdt.ls
Plug 'https://github.com/mfussenegger/nvim-jdtls'
" Customized diagnostics (warning, errors, etc.) for LSP
Plug 'https://github.com/dgagn/diagflow.nvim'

Plug 'https://github.com/ms-jpq/coq_nvim'
Plug 'https://github.com/ray-x/lsp_signature.nvim'

" Plug 'neovim/nvim-lspconfig'
" Plug 'https://github.com/hrsh7th/nvim-cmp'
" Plug 'https://github.com/hrsh7th/cmp-nvim-lsp'
" Plug 'https://github.com/hrsh7th/cmp-buffer'
" Plug 'https://github.com/hrsh7th/cmp-path'
" Plug 'https://github.com/hrsh7th/cmp-cmdline'

call plug#end()


" Vim configuration
"""""""""""""""""""
" Wrap at word boundaries. Otherwise, we'd have characters of the same word on different lines
set linebreak
" Show text at the start of a line that was wrapped. With rlet' instead of 'set', we can use multiple characters
let &showbreak='↳'

" Set Vim's current directory to the opened buffer
set autochdir

" Don't automatically load the changes of the underlying file into Vim's
" buffer. The default in Neovim is to set autoread
set noautoread

" Sets the title of the window in which Vim is running
" From https://askubuntu.com/questions/438401/how-to-display-the-name-of-file-which-i-am-currently-editing-with-vim-on-termina
" More on setting the title: https://vim.fandom.com/wiki/Automatically_set_screen_title
set title

""" Configuration for editing text

" Each indentation level is two spaces. Tabs are not used
set softtabstop=2 shiftwidth=2 expandtab

" From https://sanctum.geek.nz/cgit/dotfiles.git/tree/vim/vimrc?id=6944162a4f39daec2a76418f910e5ae0d0200a76#n127
" When indenting lines with < or >, round the indent to a multiple of
" 'shiftwidth', so even if the line is indented by one space it will indent
" up to 4 and down to 0, for example
set shiftround

" Use the tilde ~ as an operator with motions, rather than just swapping the
" case of the character under the cursor
set tildeop

set completeopt=menuone,popup

" Assembly
let asmsyntax="nasm"

""" Set colors, use :hi to see the currently used colors
let g:colorsDir = g:vimDir . "/colors/"

" Gets the highlight group under the cursor. Also shows to which other group it
" links to.
" From here:
" https://stackoverflow.com/questions/9464844/how-to-get-group-name-of-highlighting-under-cursor-in-vim#37040415
function! SynGroup()
    let l:s = synID(line('.'), col('.'), 1)
    echo synIDattr(l:s, 'name') . ' -> ' . synIDattr(synIDtrans(l:s), 'name')
endfunction

function! GetLinkTarget(group)
    let l:groupID = hlID(a:group)
    " Get the group this highlight group links to
    let l:linkedGroupID = synIDtrans(l:groupID)
    let l:linkedGroupName = synIDattr(l:linkedGroupID, 'name')
    return l:linkedGroupName
endfunction

" With a URL under the cursor, insert the title of a web page as a Markdown link.
function! InsertWebPageTitle()
  " Get the URL under the cursor
  let l:url = expand('<cWORD>')
  let l:pageTitle = system('$XDG_CONFIG_HOME/scripts/get_web_page_title_from_url.sh ' . l:url)
  " Remove the newline at the end
  let l:pageTitle = trim(l:pageTitle)

  " Replace the URL in the buffer with a Markdown link. The page title is the
  " link text, the URL is the link target
  let l:link = '[' . l:pageTitle . '](' . l:url . ')'
  execute "normal ciW" . l:link

endfunction

function! SetColors()
  " background.vim contains 'set background=dark' or 'set background=light'.
  " Since we use the Lua module 'reload_colors_on_background_change' to watch
  " that file, a script can write to it which causes the Lua module to call
  " SetColors again to adapt the colors to the new background.
  execute "source " . g:colorsDir . "background.vim"

  if &background ==# 'dark'
    " echom "Background is dark"
    let l:colorsFile = g:colorsDir . "dark.vim"
    let l:guiForeground="White"
    let l:ctermForeground="15"
  else
    " echom "Background is light"
    let l:colorsFile = g:colorsDir . "light.vim"
    let l:guiForeground="Black"
    let l:ctermForeground="15"
  endif

  " From here:
  " https://vi.stackexchange.com/questions/14812/how-to-edit-all-vim-highlight-groups
  for l:highlightGroup in getcompletion('','highlight')

    " Get the group this highlight group links to
    let l:linkedGroup = GetLinkTarget(l:highlightGroup)
    " Only set the root highlight groups (those that don't link to others).
    " This leaves links intact
    if l:highlightGroup ==# l:linkedGroup
      execute "hi " . l:highlightGroup . " ctermfg=" . l:ctermForeground . " ctermbg=none guifg=" . l:guiForeground . " guibg=none"
    endif
  endfor

  execute "source " . l:colorsFile
  execute "source " . g:colorsDir . "shared.vim"

endfunction

" Setup colors when init.vim runs
call SetColors()

" Located in ~/.config/nvim/lua/
lua require('reload_colors_on_background_change')

" Display end of line characters, tabs, and characters for trailing whitespace in normal mode
set list
augroup ListCharactersGroup
  autocmd!
  autocmd InsertLeave * set list
  " Don't show trailing whitespace while editing since this would show a
  " character each time when beginning a new word
  autocmd InsertEnter * set nolist
augroup end

" Set the characters signifying tabs, trailing spaces, and non-breaking spaces
" (if 'list' is enabled using 'set list')
set listchars=tab:↹·,trail:$,nbsp:⎵

" No lines to separate the vertical splits.
" We set a different background for unfocused splits with 'NormalNC'
set fillchars+=vert:\ 

" Second last line gives info about current file and cursor position.
" %= is for right aligning the current line position, column and percentage.
" See https://stackoverflow.com/questions/4322004/using-vim-how-do-i-set-statusline-to-align-right
" %< is a truncation marker. It tells Vim to truncate the section of the statusline to the left of it if the statusline becomes too long to fit in the window.
" %r shows if a file is read-only
" %y shows the file type, for example '[vim]'
" %l is the current line number
" %c is the current column number
" %p percentage through file in lines
" %L is the total number of lines in the file
set statusline=%<%F%m%r%=%{%FileTypeEncodingAndFormat()%}%l,%c\ %p%%\ of\ %L%{%LspStatus()%}

function! LspStatus()
  " Call get_lsp_status() in file `lua/statusline.lua`
  return luaeval("require('statusline').get_lsp_status()")
endfunction

function! LspStatusSimple()
   " Get clients for the current buffer
    let l:clients = luaeval("#vim.lsp.buf_get_clients()")
    return len(l:clients) > 0 ? "🟢" : "⚪"
    " Alternatively use `:lua =vim.lsp.buf_is_attached(0, 1)` to check if the
    " current buffer is attached to a language server
endfunction

" Return the filetype, encoding, and format (Shell script, Rust file, Markdown, etc.) in the statusline
function! FileTypeEncodingAndFormat()
" %{%FileTypeIcon()%}\ %{%FileEncodingHighlight()%}\ %{%FileFormatHighlight()%}
" If the $TERM is linux, the delimiter is | otherwise it's ｜
  let l:delimiter = $TERM ==# 'linux' ? ' | ' : '｜'
  return l:delimiter . "%{FileTypeIcon()} %{FileEncodingHighlight()} %{FileFormatHighlight()}" . l:delimiter
endfunction

function! FileTypeIcon()
  " Used this to find icons: https://www.nerdfonts.com/cheat-sheet
  " On Arch, the icons are available in package 'ttf-nerd-fonts-symbols'
  let l:icons = {
        \ 'c': '',
        \ 'cpp': '',
        \ 'css': '',
        \ 'dockerfile': '',
        \ 'git': '',
        \ 'go': '',
        \ 'html': '',
        \ 'java': '',
        \ 'javascript': '',
        \ 'kotlin': '',
        \ 'lua': '',
        \ 'make': '',
        \ 'markdown': '',
        \ 'perl': '',
        \ 'php': '',
        \ 'python': '',
        \ 'ruby': '',
        \ 'rust': '',
        \ 'sh': '',
        \ 'svg': '󰜡',
        \ 'tex': '',
        \ 'text': '',
        \ 'toml': '',
        \ 'vim': '',
        \ 'yaml': '',
        \ }

  " Get the filetype
  let l:filetype = &filetype

  " Check if an icon exists, otherwise return the filetype
  " Also check if $TERM is 'linux' since we can't use Nerd Fonts in a virtual
  " console
  return has_key(l:icons, l:filetype) && $TERM !=# 'linux' ? l:icons[l:filetype] : l:filetype
endfunction

" Return the output of &fileformat which is based on the file's line endings ('unix', 'dos', or 'mac'). If it's 'unix', return it as is, otherwise highlight it
function! FileFormatHighlight()
  " Alternatively to the Nerd Font icon, use the penguin emoji: 🐧
  let l:penguin = ""

  let l:fformat = &fileformat
  if l:fformat ==# "unix"
    " Check if we're in a virtual console instead of a terminal emulator like
    " kitty, we can't use Nerd Fonts or emojis there
    if $TERM ==# "linux"
        return l:fformat
      else
        return l:penguin
    endif
  else
    " %* Resets the color to the default
    return "%#StatusLineWarning#" . l:fformat . "%*"
  endif
endfunction

function! FileEncodingHighlight()
  " Get the file encoding or fallback to Neovim's internal encoding
  let l:encoding = &fileencoding !=# '' ? &fileencoding : &encoding
  " Apply custom highlighting if encoding is not utf-8
  if l:encoding ==# 'utf-8'
    if $TERM ==# "linux"
      return l:encoding
    else
      " 🌐
      return "󰻐"
    endif
  else
    " %* Resets the color to the default
    return "%#StatusLineWarning#" . l:encoding . "%*"
  endif
endfunction



""" Configure text search
" Ignore case when searching
set ignorecase

" Become case sensitive if you type uppercase characters
set smartcase

" Enables persistent undo: Changes can be undone after closing and reopening a file
" The default undodir is in ~/.local/state/nvim/undo
set undofile

""" Key mappings and command abbreviations
""""""""""""""""""""""""""""""""""""""""""
" The space bar is big and easy to reach → Make it the leader key
let mapleader = ' '

" Control u deletes text in insert mode without the possibility
" of undoing the deletion. This is dangerous, thus we remap that combination to
" jump to the end of the line
imap <c-u> <c-o>A

" Close the current buffer
nmap <leader>q :bd<cr>

" Replaces the first incorrectly spelled word with the first suggestion in the
" dictionary. From here: https://castel.dev/post/lecture-notes-1/:
" "It basically jump to the previous spelling mistake [s, then picks the first
" suggestion 1z=, and then jumps back `]a. The <c-g>u in the middle make it
" possible to undo the spelling quickly."
" already
inoremap <c-s> <c-g>u<Esc>[s1z=`]a<c-g>u

nmap <Esc><Esc> :w<cr>

" Quickfix list
" Go to the previous location
nnoremap [q :cprev<cr>
" Go to the next location
nnoremap ]q :cnext<cr>

" We need to save C-I before adding a mapping for tab:
" https://github.com/csdvrx/CuteVim/blob/15fed4e173121de20dfeed9983d5568ee00b7e8c/.vimrc#L283C1-L283C21
nnoremap <C-i> <C-i>
" Ctrl+^ switches to the previous buffer
nmap <Tab> <c-^>

" Create a new line below the current one and move the cursor back to the
" original position
nmap <C-n> m`o<Esc>``

" Delete all buffers but the current one
" https://stackoverflow.com/questions/4545275/vim-close-all-buffers-but-this-one#42071865
nmap <F9> :%bd <bar> e#<cr>

" Go through the files history via a tree of changes
nnoremap <F5> :UndotreeToggle<cr>

" Repeat the last command from Vim's commandline: https://vim.wikia.com/wiki/Repeat_last_colon_command
nmap <leader>. @:
vmap <leader>. @:

" Rename current word in entire file. Ask for confirmation every time.
" Use register 9 to store the current word
nnoremap gr "9yiw:%s/<c-r>9//gc<left><left><left>

" Highlight the occurrences of the current word (without going anywhere else)
" Alternatively: https://github.com/RRethy/vim-illuminate
nnoremap <leader>cw *N

nnoremap <leader>f :vert Vifm<cr>

" Highlight yanked text. See also:
" https://neovim.io/doc/user/lua.html#vim.highlight
augroup HighlightYank
  autocmd!
  autocmd TextYankPost * silent! lua vim.highlight.on_yank {timeout=300}
augroup END

" Yank text defined by motion into clipboard register. Usage:
" <leader>y{motion}
" For example <leader>yi' will yank the text inside the quotes to the
" clipboard
nmap <silent> <leader>y :set opfunc=ClipboardYank<cr>g@
function! ClipboardYank(type, ...)
  execute "normal! `[v`]\"+y"
endfunction

" Yank the current line to the clipboard
nmap <leader>yy "+yy

" Yank the rest of the line to the clipboard
nmap <leader>Y "+Y

" Yank what's currently selected to the clipboard
vmap <leader>y "+y

" Paste the clipboard content
nmap <leader>p "+p

" Select the text you just pasted
nmap <leader>s `[v`]

" Put the absolute path of the current file into the system clipboard.
" Convert a path starting with a home directory ("home/mb") to a tilde.
command! YankPath :let @+=system('$XDG_CONFIG_HOME/scripts/home_dir_to_tilde.sh ' . shellescape(expand('%:p')))

" Paste the contents of the clipboard register
" <c-g>u Adds a new change so we can undo the pasting without undoing
" what we've written before
imap <c-r><c-r> <c-g>u<c-r>+
cmap <c-r><c-r> <c-r>+

" Add the current date
" https://stackoverflow.com/questions/56052/best-way-to-insert-timestamp-in-vim#58604
nmap <F3> i<C-R>=strftime("%Y-%m-%d")<cr><Esc>
imap <F3> <C-R>=strftime("%Y-%m-%d")<cr>
" %A is the current week day. <F15> is Shift + F3
nmap <F15> i<C-R>=strftime("%A, %Y-%m-%d")<cr><Esc>
imap <F15> <C-R>=strftime("%A, %Y-%m-%d")<cr>

" <F51> is Alt + F3
nmap <F51> i<C-R>=strftime("%H:%M:%S")<cr><Esc>
imap <F51> <C-R>=strftime("%H:%M:%S")<cr>

" Unhighlight search results as well as spelling mistakes and refresh the screen
nmap <silent> <leader>cc :nohl <bar> set nospell <bar> redraw!<cr>

" If lines are long and being wrapped, these shortcuts make the movement within
" them more natural
nnoremap <silent> j gj
nnoremap <silent> k gk
vnoremap <silent> j gj
vnoremap <silent> k gk

" Inserts the text on the clipboard as a link target into the text, using the
" previous word as the link text: [word](https://link.target)
" The link is in Markdown, but we use it here instead of ftplugin/markdown
" since I want to be able to use it when writing question/answers on Stack
" Exchange using Vim which uses .txt files
inoremap <c-]> <c-g>u<Esc>Bi[<Esc>Ea](<c-r>+)
" Make the currently selected text the text of a Markdown link. The URL is in
" the clipboard, register '+'. When hitting 'c' in visual mode, the deleted text
" will be in register '"'.
vnoremap <c-]> c[<c-r>"](<c-r>+)<esc>

" Search inside visually selected text, from here:
" https://vim.fandom.com/wiki/Search_and_replace_in_a_visual_selection
vnoremap <M-/> <Esc>/\%V

" New map for omnicompletion since <c-x><c-o> is cumbersome to type
inoremap <c-space> <c-x><c-o>

" With the cursor on a URL, insert the title of the web page as a Markdown
" link
nmap <leader>it :call InsertWebPageTitle()<cr>

" Move between splits using Alt + h/j/k/l
nnoremap <A-h> <C-w>h
nnoremap <A-j> <C-w>j
nnoremap <A-k> <C-w>k
nnoremap <A-l> <C-w>l
" Close the split
nnoremap <A-q> <C-w>c

" Use alt tab to switch between current and previous split
nnoremap <A-Tab> <C-w>w

" Enter split resize mode
nnoremap <A-r> :call ResizeMode()<cr>

function! ResizeMode()
  echo "Resize Mode: h/j/k/l to resize. ESC or Enter to exit"
  nnoremap <silent> h :silent! vertical resize -2<cr>
  nnoremap <silent> l :silent! vertical resize +2<cr>
  nnoremap <silent> k :silent! resize -2<cr>
  nnoremap <silent> j :silent! resize +2<cr>
  nnoremap <silent> <Esc> :call ResizeModeEnd()<cr>
  nnoremap <silent> <cr> :call ResizeModeEnd()<cr>
endfunction

function! ResizeModeEnd()
  echo "Exiting split resize mode"
  nunmap h
  nunmap l
  nunmap k
  nunmap j
  nunmap <Esc>
  nunmap <cr>
endfunction

" Command abbreviations for spell checking
" setlocal applies the changes to just the current buffer
ca spellen setlocal spell spelllang=en
ca spellde setlocal spell spelllang=de

""" Plugin configuration
""""""""""""""""""""""""
""" fzf Vim plugin
" Keep in mind that :Files uses the environment variable FZF_DEFAULT_COMMAND to search files

" Find files by name under a directory specified as the first argument and
" exclude the directories specified in the following arguments
" See: https://vimdoc.sourceforge.net/htmldoc/map.html#%3Cf-args%3E
command! -nargs=+ -complete=dir FilesWithIgnore call s:FilesWithIgnore(<f-args>)

" Find files by name under the current directory
nmap <leader>gh :Files!<cr>
" Find files by name under a given directory
nmap <silent> <leader>gc :FilesWithIgnore ~/.config Signal chromium JetBrains libreoffice<cr>
nmap <silent> <leader>gp :FilesWithIgnore ~/dev/ target<cr>

nmap <leader>gd :Files! ~/docs<cr>
nmap <leader>gj :Files! ~/job/<cr>
nmap <leader>gs :Files! ~/secrets/<cr>
nmap <leader>gw :Files! ~/websites/<cr>

" Find files by name starting at startDir and excluding the directories
" specified in the following arguments (...)
function! s:FilesWithIgnore(startDir, ...)
  let l:dirsToExclude = a:000
  let l:fzfCommand = '$FZF_DEFAULT_COMMAND . ' . a:startDir
  for l:excludedDir in l:dirsToExclude
    let l:fzfCommand .= ' --exclude ' . l:excludedDir
  endfor
  call fzf#run({'source': l:fzfCommand, 'sink': 'e'})
endfunction

" Search files by name in current Git repo, include untracked files with --others
" https://github.com/junegunn/fzf.vim/issues/121
nmap <leader>gr :GFiles! --cached --others --exclude-standard<cr>

" Fuzzy search in buffers and switch
nmap <leader>b :Buffers<cr>

" Search content in the current file
" Alternative: Telescope current_buffer_fuzzy_find
nmap <leader>l :BLinesWithPreview!<cr>

" Search content in the current file and in files in and under the current directory
nmap <leader>G :Rg!<cr>
" Search history of Ex commands
nmap <leader>e :History:<cr>
" Show and open most recently used files
nmap <leader>r :History!<cr>

" Search content from the root of the current Git repo
nmap <leader>o :RgIn! `=GetGitRoot()`<cr>
" Search current word in Git repo. Adapted from here: https://news.ycombinator.com/item?id=26634419#26635204
nnoremap <C-Space> yiw:RgIn! `=GetGitRoot()` <C-r>"<cr>
" Search current selection in Git repo
vnoremap <C-Space> y:RgIn! `=GetGitRoot()` <C-r>"<cr>

" File path completion
imap <c-x><c-f> <plug>(fzf-complete-path)
" Complete whole lines
imap <c-x><c-l> <plug>(fzf-complete-line)

" Gets the root of the Git repo or submodule, relative to the current buffer
function! GetGitRoot()
  " From here:
  " https://github.com/junegunn/fzf.vim/issues/27#issuecomment-185824237
  " Here's a way to detect project roots that don't use Git:
  " https://github.com/airblade/vim-rooter
  return systemlist('git -C ' . shellescape(expand('%:p:h')) . ' rev-parse --show-toplevel')[0]
endfunction

" RgIn: Start ripgrep in the specified directory, filter with fzf.
" See also: https://github.com/junegunn/fzf.vim/issues/837
"
" Usage
"   :RgIn startDir search_term
"
" If the command was called with a bang ("RgIn!"), make the search window
" fullscreen
function! s:rg_in(showFullscreen, ...)
  let l:startDir=expand(a:1)

  if !isdirectory(l:startDir)
    throw 'not a valid directory: ' .. l:startDir
  endif

  " a:000 contains the argument list → Join the arguments after the first one
  " Escaping the pattern also converts the empty string to '' which is
  " necessary for succesfully calling ripgrep later. This is relevant when
  " callling s:rg_in without an initial pattern via ':RgIn! `=GetGitRoot()`',for example
  let l:pattern = shellescape(join(a:000[1:], ' '))
  let l:rgCommand = "rg --color=never --line-number --ignore-case " .. l:pattern
  let l:hasColumn = 0
  " Setting the start directory here instead of in rgCommand because then the
  " path of the start directory is not shown in the search results
  let l:fzfCommand = fzf#vim#with_preview({'dir': l:startDir, 'options': '--layout reverse'}, 'down:50%')
  call fzf#vim#grep(l:rgCommand, l:hasColumn, l:fzfCommand, a:showFullscreen)

endfunction
" See this: https://vi.stackexchange.com/questions/13965/what-is-command-bang-nargs-in-a-vimrc-file
" -bang: The command can also be called as 'RgIn!' to make the search window fullscreen
" <bang>0: If there is no bang, pass 0 to the function, otherwise 1
command! -bang -nargs=+ -complete=dir RgIn call s:rg_in(<bang>0, <f-args>)

" From https://github.com/junegunn/fzf.vim/issues/374#issuecomment-724301156
" We need --with-filename to allow jumping to the line in the file
" Using '--with-nth=2..' we hide the file path and only show the line number and
" the matched line
command! -bang -nargs=* BLinesWithPreview
    \ call fzf#vim#grep(
    \   'rg --with-filename --line-number --smart-case . '.fnameescape(expand('%:p')), 1,
    \   fzf#vim#with_preview({'options': '--layout reverse --query '.shellescape(<q-args>).' --with-nth=2.. --delimiter=":"'}, 'down:50%'))

" GitHub Copilot
imap <c-l> <Plug>(copilot-accept-word)

let g:copilot_filetypes = {
      \ 'text': v:false,
      \ 'markdown': v:false,
      \ 'mail': v:false,
      \ }

" Markdown Preview

" Set to 1, Vim will refresh Markdown when saving the buffer or
" when leaving insert mode. Default 0 is auto-refresh Markdown as you edit or
" move the cursor
" default: 0
let g:mkdp_refresh_slow = 1

" Don't automatically close the tab with Markdown preview if switching away
" from the Vim buffer
let g:mkdp_auto_close = 0

" vim-table-mode
" https://github.com/dhruvasagar/vim-table-mode/issues/227
let g:table_mode_syntax = 0

" which-key.nvim
nnoremap <leader>? :Telescope keymaps<cr>

" Rainbow parentheses
let g:rainbow#pairs = [['(', ')'], ['[', ']'], ['{', '}']]
let g:rainbow#colors = {
\   'dark': [
\     ['yellow',  'orange1'     ],
\     ['green',   'yellow1'     ],
\     ['cyan',    'greenyellow' ],
\     ['magenta', 'green1'      ],
\     ['red',     'springgreen1'],
\     ['yellow',  'cyan1'       ],
\     ['green',   'slateblue1'  ],
\     ['cyan',    'magenta1'    ],
\     ['magenta', 'purple1'     ]
\   ],
\   'light': [
\     ['darkyellow',  'orangered3'    ],
\     ['darkgreen',   'orange2'       ],
\     ['blue',        'yellow3'       ],
\     ['darkmagenta', 'olivedrab4'    ],
\     ['red',         'green4'        ],
\     ['darkyellow',  'paleturquoise3'],
\     ['darkgreen',   'deepskyblue4'  ],
\     ['blue',        'darkslateblue' ],
\     ['darkmagenta', 'darkviolet'    ]
\   ]
\ }

" vifm.vim
" Allows vifm to be opened as a sidebar with ':vert Vifm'
let g:vifm_embed_split=1

" EasyMotion
" Don't shade (dehighlight) the text when EasyMotion is active
" If we do want to change the color of the shaded text,
" the colors can be set with 'hi EasyMotionShadeDefault ctermfg=gray
" guifg=gray'
let g:EasyMotion_do_shade=0

" LSP
nmap <leader>drn :lua vim.lsp.buf.rename()<cr>
" Maybe use <Cmd> instead of having a map for normal and visual mode:
" https://neovim.io/doc/user/map.html#%3CCmd%3E
nmap <leader>dca :lua vim.lsp.buf.code_action()<cr>
vmap <leader>dca :lua vim.lsp.buf.code_action()<cr>
" List call sites of the function under the cursor
nmap <leader>dic :Telescope lsp_incoming_calls<cr>
" Get references to the symbol under the cursor
" nmap <leader>dgr :lua vim.lsp.buf.references()<cr>
nmap <leader>dgr :Telescope lsp_references<cr>
" List functions of the current file
nmap <leader>dlf :lua require('telescope.builtin').lsp_document_symbols({ symbols='function' })<cr>
" Format the source code (indentation, etc.)
" Note that gq should be able to do this as well
nmap <leader>dfm :lua vim.lsp.buf.format()<cr>
nmap <leader>ddf :lua vim.diagnostic.open_float()<cr>

" Predefined key mappings for LSP
" ]d and [d go to the next and previous diagnostic
" K shows the documentation of the symbol under the cursor
" ctrl + ] goes to the definition of the symbol under the cursor

lua require('diagflow').setup()
