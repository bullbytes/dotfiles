" Text color
hi Normal guifg=#24292F
" Colors for highlighting search results
" Previous background : #d9f9f6
hi Search ctermfg=0 ctermbg=10 guifg=White guibg=#671FF0

" The currently selected search result (jump to the next result with 'p')
hi CurSearch ctermfg=7 gui=bold guifg=White guibg=#671FF0

" Visually selected text
hi Visual cterm=bold ctermfg=0 ctermbg=4 gui=bold guifg=White guibg=Black

" Also for markdown heading
hi Title guifg=SeaGreen

"#5158FF
hi Comment ctermfg=Blue guifg=#4334FF
" Used for vimCommentTitle for example
hi PreProc guifg=#FF5F00

hi String guifg=#AF24FF

hi Statement gui=none guifg=Navy
hi! link Function Statement

" Used for the text 'Type gui guifg PapayaWhip' in 'hi Type gui=none guifg=PapayaWhip'
hi Type gui=none guifg=#046304

hi Identifier guifg=Teal

" In Markdown, the URL in links uses this color
hi markdownUrl ctermfg=4 guifg=#3A81C3

" This is the gutter column on the left that shows symbols for compiler errors, for example. See ':help signs' for more
hi SignColumn ctermbg=black ctermfg=none guibg=#fdf7dd
" The line number column, if 'signcolumn = "number"' is set, this defines also
" the color of the line sign column
hi LineNr ctermfg=0 guifg=#1D438A

" Statusline of the focused window
hi StatusLine cterm=none ctermbg=White ctermfg=Black guibg=#00755F guifg=White
" Statusline of the non-focused window
hi StatusLineNC cterm=none ctermbg=LightGray ctermfg=White guibg=Gray guifg=White

" Used to highlight worrisome parts in the statusline
hi StatusLineWarning ctermfg=0 ctermbg=3 guifg=White guibg=#e57702

" The color of a non-current buffer
" #FDF6FF
hi NormalNC guibg=#FFFCF2

" Also used by fzf.vim for the buffer number when selecting buffers.
" fzf's buffer number color is E69E00 if Constant is red but uses the same
" color as Constant if it's #965E30 or #00755F
hi Constant guifg=#965E30
"
" LSP inlay warnings, errors, hints, etc.
hi DiagnosticError guifg=#FF4069
hi DiagnosticWarn guifg=DarkOrange
hi DiagnosticInfo guifg=Blue
hi DiagnosticHint guifg=Green
hi DiagnosticUnnecessary gui=none guifg=Gray

" When using LSP, text is categorized into different types. Get the
" type of the text under the cursor with `:Inspect`.
" hi @lsp.type.class guifg=#003B4A
hi! link @lsp.type.class Type
hi @lsp.type.record guifg=#FF26CA
hi @lsp.type.modifier.java guifg=#288AAD
hi javaExternal guifg=#097A56


" The styling of code that triggers warnings, errors, etc.
hi DiagnosticUnderlineError gui=undercurl guisp=#FF4069
" The Java LSP (with jdtls) doesn't seem to use DiagnosticUnderlineWarn
hi DiagnosticUnderlineWarn gui=undercurl guisp=DarkOrange
hi DiagnosticUnderlineInfo gui=none guifg=Blue
hi DiagnosticUnderlineHint gui=underdotted guifg=Green

" Colors if, fi, then, else, do, done
hi shConditional guifg=#2691C7

" Looks good: Foreground FFC745, background 007A78
