" To set the cursor color, consult the config of the terminal emulator or see
" this: https://stackoverflow.com/questions/6230490/how-i-can-change-cursor-color-in-color-scheme-vim
"
" See this for color names: https://www.w3schools.com/cssref/css_colors.php

" Used for trailing whitespace and visualizing line breaks
hi NonText gui=bold guifg=LightSlateGray

" Highlight the matching parenthesis. Don't change the foreground color (use
" the current one)
hi MatchParen cterm=bold gui=bold ctermfg=none guifg=none

hi SpellBad cterm=underline ctermfg=1 ctermbg=none guisp=#FF4069 gui=undercurl

" Shows information about folds
hi Folded guifg=Black guibg=White

" Popup menu, also for code autocompletion
hi PMenu ctermbg=8 ctermfg=0 guibg=FloralWhite guifg=Black
hi PmenuSel ctermbg=4 ctermfg=0 cterm=none guibg=#2AA1AE guifg=White

" Current line when debugging using :Termdebug
"hi debugPC ctermbg=darkyellow ctermfg=white guibg=#AFBE93 guifg=Black
"hi debugBreakpoint ctermbg=3 ctermfg=0 guibg=#867E36 guifg=white

" Define the colors for 'set cursorline' that highlight's the current line
hi CursorLine cterm=reverse gui=reverse
" Same for 'set cursorcolumn'
hi CursorColumn cterm=reverse gui=reverse

" Also used by FoldCol
hi Conceal ctermbg=yellow ctermfg=black guibg=yellow guifg=black

" Highlights for GitHub Copilot suggestions
hi CopilotSuggestion guifg=#EE1466 ctermfg=5


hi link markdownCodeBlock markdownCode
hi link markdownCode Type

" Used in documentation shown via LSP
hi link @markup.raw.markdown_inline Statement
hi link @markup.raw.block.markdown Statement


hi Special guifg=#FF00B8

hi Operator guifg=#007C8C

" EasyMotion (jump around in the buffer)
hi EasyMotionTargetDefault cterm=bold ctermfg=196 gui=bold guifg=#ff00f0
hi EasyMotionTarget2FirstDefault cterm=bold ctermfg=11 gui=bold guifg=#ff00f0
hi EasyMotionTarget2SecondDefault cterm=bold ctermfg=3 gui=bold guifg=#ff00f0
" Color of the text when EasyMotion is active
hi EasyMotionShadeDefault ctermfg=white guifg=white
