" Colors for highlighting search results
hi Search ctermfg=0 ctermbg=10 guifg=White guibg=#671FF0

" The currently selected search result (jump to the next result with 'p')
hi CurSearch ctermfg=7 gui=bold guifg=White guibg=#671FF0

" Visually selected text
hi Visual cterm=bold ctermfg=0 ctermbg=15 gui=bold guifg=Black guibg=White

" Also for markdown heading
hi Title guifg=#22C55E

hi Comment ctermfg=Blue guifg=#FFCD9F

" Used for vimCommentTitle for example
hi PreProc guifg=Orange

hi String guifg=#c284e0

hi Statement gui=none guifg=PowderBlue
hi! link Function Statement

" Used for the text 'Type gui guifg PapayaWhip' in 'hi Type gui=none guifg=PapayaWhip'
hi Type gui=none guifg=PapayaWhip

hi Identifier guifg=PapayaWhip

" In Markdown, the URL in links uses this color
hi markdownUrl ctermfg=4 guifg=#cfdddc

" This is the gutter column on the left that shows symbols for compiler errors, for example. See ':help signs' for more
hi SignColumn ctermbg=black ctermfg=none guibg=#070707

hi! link FoldColumn SignColumn

hi StatusLine cterm=none ctermbg=Black ctermfg=White guibg=Black guifg=White

" Statusline of the focused window
hi StatusLine cterm=none ctermbg=White ctermfg=Black guibg=Black guifg=#c5f8ff
" Statusline of the non-focused window
hi StatusLineNC cterm=none ctermbg=LightGray ctermfg=White guibg=#3b4046 guifg=White

" Used to highlight worrisome parts in the statusline
hi StatusLineWarning ctermfg=0 ctermbg=3 guifg=White guibg=#e57702

hi NormalNC guibg=#646050
