#!/usr/bin/python3
# Reads an email from standard in and prints the sender's email address.
# E.g.: sender@domain.com
# Call it from Bash like this: `./get_sender_email.py < your_email`

import sys
import email
from email import policy

# Read the email from standard in
msg = email.message_from_file(sys.stdin, policy=policy.default)

# See https://docs.python.org/3/library/email.headerregistry.html
sender_email_address = msg['from'].addresses[0].addr_spec

print(sender_email_address)
