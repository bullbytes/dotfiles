# Here are examples of fzf configuration:
# https://github.com/junegunn/fzf/wiki/Examples
# https://github.com/samoshkin/dotfiles/blob/master/fzf.sh


# Creates keybindings and defines functions for fzf.
# For example, Alt+c fuzzy searches directories and changes to them.
# Another example: Ctrl+t fuzzy searches file names and puts them on the shell prompt
[[ -d "/usr/share/fzf/" ]] && source /usr/share/fzf/completion.bash && source /usr/share/fzf/key-bindings.bash

# Support path completion for these commands as well. E.g., "zathura ~/**<tab>`
# We alias our $EDITOR to e
_fzf_setup_completion path zathura imv trash feh libreoffice firefox vlc e

# Only complete directories, not files
_fzf_setup_completion dir tree

# Fuzzy-search processes and pass their ID to a program
_fzf_setup_completion proc top pmap pstree

# Putting tree last in the list of commands since it doesn't return a non-zero exit code when trying to open a non-directory
# We only load the first 200 lines of the file which enables fast previews of large text files.
# Requires: pacman -S poppler
previewCmd="(pdftotext -nopgbrk {} - || cat {} || tree -C {}) 2> /dev/null | head -200"

# These options are also used with Alt+c
# Show search input and number of filtered entry on one line with "--info=inline"
# Hide the preview initially
export FZF_DEFAULT_OPTS="--info=inline --cycle --preview \"$previewCmd\"
                         --preview-window=hidden:sharp
                         --bind ctrl-h:toggle-preview,ctrl-y:preview-up,ctrl-e:preview-down
                         --bind 'ctrl-c:execute-silent(printf {} | cut -f 2- |& wl-copy)'
                         "

# Use fd instead of find. Show hidden files (starting with dot).
# Per default, fd ignores files in a Git repo that are ignored via .gitignore. Use --no-ignore to change that
export FZF_DEFAULT_COMMAND='fd --type file --type symlink --no-ignore --hidden --exclude .git --exclude .stversions'

# Show a preview with tree when fuzzy-searching directories
export FZF_ALT_C_OPTS="--preview 'tree -C {} | head -200'"

# Fuzzy search a file with fzf's default command (e.g., fd) and fzf from a start directory
fzf_from(){
  local startDir="$1"
  $FZF_DEFAULT_COMMAND . "$startDir" | fzf
}

# Starts searching from a start directory (first and only parameter) and opens the selected file with xdg-open.
# Define the default application of file types in `~/.config/mimeapps.list`.
# Get the MIME type of a file with `xdg-mime query filetype file_name`.
#
# This function is used in inputrc.
fzf_search_from_dir_then_open(){
  local startDir="$1"

  # Contains the file path if we selected a file with fzf, is empty if
  # we cancelled the search with escape
  local fileMaybe=$(fzf_from "$startDir")
  if [ -n "$fileMaybe" ]; then
    xdg-open "$fileMaybe"
  fi
}

# One parameter: The directory from where fzf starts searching
# This function is used in inputrc
fzf_cd () {
  local original_dir="$(pwd)"
  local start_dir=$(realpath "$1")
  cd "$start_dir"
  local chosen_file=$(fzf)

  # If we didn't pick a file with fzf, change back to the original directory we were in before the search. Otherwise, change into the dir of the chosen file
  test -z "$chosen_file" && cd "$original_dir" || cd "$(dirname "$chosen_file")"
}

# fuzzy grep via ripgrep, then open at line with nvim
# See also:
# https://github.com/junegunn/fzf/wiki/Examples#user-content-opening-files
search_content_with_fzf_then_open_in_editor() {
  local file
  local line

  # rg: We use a dot for rg to search for everything, then refine it with fzf
  # fzf: -0 means "exit immediately when there's no match. -1: automatically select the only match
  # awk: gg's output is separated by colons like this: "file_name:line_nr:matched line"

  read -r file line <<<"$(rg --hidden --color=never --line-number --no-heading --smart-case . | fzf -0 -1 | awk -F: '{print $1, $2}')"

  # Do nothing if we didn't pick a file. Otherwise, open the file at the matched line with Neovim
  test -z $file || nvim $file +$line
}

