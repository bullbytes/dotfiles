if [ $TERM == "linux" ]; then
  # Set the looks of the "linux" terminal, the one built into the kernel you get from /usr/bin/login
  # This configures the colors and cursor shape of the Linux terminal. Terminal emulators configure the cursor separately

  # A non-blinking block
  cursor_shape=16

  # When we move the cursor on a character, this is the color the character will have.
  # For available colors, see this: https://unix.stackexchange.com/questions/124407/what-color-codes-can-i-use-in-my-bash-ps1-prompt
  cursor_foreground=7
  cursor_background=228

  # In inputrc, we also set the cursor shape. But inputrc is not loaded until starting a terminal emulator. So this configuration is important for the "linux" terminal
  box_no_blink="\033[?${cursor_shape};${cursor_foreground};${cursor_background}c"

  # Setting PS1 also affects the prompt when using a terminal emulator
  # If we set PS1 in `.bash_profile`, only the login shell would have the PS1 value we set there. When
  # spawning a new Bash instance, it would use the default PS1 value of \s-\v\$
  # See also this: https://unix.stackexchange.com/questions/549070/what-is-the-correct-location-for-ps1-shell-variable
  PS1="\[$box_no_blink\]$ "

else
  PS1="$ "
fi

# Don't put a command into the history if it's identical to the previous one
HISTCONTROL=ignoredups
# Don't truncate the history in memory or on disk
HISTSIZE=-1
# After each command, write it to the history file
PROMPT_COMMAND="history -a;"

# After each command finishes, we set the terminal window title to the
# current working directory. If, for example, Vim sets the window title
# to the file's name and we exit Vim, the window title becomes the
# current directory again.
# These escape sequences are documented in man 4 console_codes.
# \033 is octal for ESC in ASCII, \007 is BEL
# Note on the use of $'\n': This allows escape sequences to be used in the PROMPT_COMMAND variable and be interpreted by the shell. We add a newline to run the next command
PROMPT_COMMAND="${PROMPT_COMMAND:+$PROMPT_COMMAND$'\n'}printf \"\033]0;\$(pwd)\007\""

# Prevents accidentally freezing the shell with Ctrl-s
# From https://unix.stackexchange.com/questions/12107/how-to-unfreeze-after-accidentally-pressing-ctrl-s-in-a-terminal
# If Bash is not connected to a PTY (which we check with tty -s), we would get the error "stty: 'standard input': Inappropriate ioctl for device"
if tty -s; then
  stty -ixon
fi

# It seems Bash aliases are not copied to child Bash instances
source "$XDG_CONFIG_HOME"/.bash_aliases

# Source the script for Bash completions (e.g., completing a command's options). Requires https://github.com/scop/bash-completion
# See https://wiki.archlinux.org/title/Bash#Tab_completion
source /usr/share/bash-completion/bash_completion

# Source fzf's completions after the ones of bash_completion to keep, e.g., "cd **" working
source "$XDG_CONFIG_HOME"/fzf.bash

### Functions (for convenience) ###
###################################

# Puts the full path of a file on the clipboard
yankpath() {
  filepath=$(realpath "$1")
  $XDG_CONFIG_HOME/scripts/home_dir_to_tilde.sh "$filepath" | wl-copy
}

# Executes a command without letting it write to stdout or stderr.
# Command runs in the background and won't receive SIGHUP: This allows closing the terminal window from which the command was run.
#
# For example: silently_and_in_background feh ~/image.jpg
silently_and_in_background() {
  cmd="$@"
  nohup $cmd &>/dev/null &
}
