#!/usr/bin/bash
dir_of_this_script=$(dirname "$0")
source "$dir_of_this_script"/utils.sh

# Usage: show_location_with_osm.sh image.jpg
# Gets the GPS location stored in the JPG and shows that location on a map using $BROWSER and OpenStreetMap.

# Exit the script immediately when a command fails and treat unset variables as errors
set -eu

img_file="$1"

# Looks like this: "GPS Position                    : 48.2793503 N, 14.2818098 E"
exiftool_output=$(exiftool -c '%.7f' -GPSPosition "$img_file")

# Get everything after the first occurrence of ": ", now we have "48.2793503 N, 14.2818098 E", for example
lat_and_long=${exiftool_output#*: }

# Split the string at the comma to separate latitude and longitude
readarray -d , -t lat_long_separated <<< "$lat_and_long"

# E.g.: "48.2793503 N"
lat=${lat_long_separated[0]}
# The longitude string starts with a space and ends with a newline → Trim that away
# E.g.: "14.2818098 E"
long=$(trim "${lat_long_separated[1]}")

lat_last_char=${lat: -1}
long_last_char=${long: -1}

lat_minus_maybe=""
if [ "$lat_last_char" != "N" ]; then lat_minus_maybe="-"; fi
long_minus_maybe=""
if [ "$long_last_char" != "E" ]; then long_minus_maybe="-"; fi

# Prepend the minus if the latitude is south of the equator and remove everything after and including the last space, such as " N". Now the latitude is "48.2793503", for example
lat=${lat_minus_maybe}${lat% *}

# Prepend the minus if the longitude is west of Greenwich and remove everything after and including the last space, such as " E". Now the longitude is "14.2818098", for example
long=${long_minus_maybe}${long% *}

# The higher the zoom level is the closer we zoom in on the map
zoom_level=19

# mlat and mlon define the location of the marker
# See https://wiki.openstreetmap.org/wiki/Browsing#Other_URL_tricks
osm_url="https://www.openstreetmap.org/?mlat=${lat}&mlon=${long}#map=${zoom_level}/${lat}/${long}"

echo "URL is $osm_url"

$BROWSER "$osm_url" &

