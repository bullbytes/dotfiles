#!/bin/bash

# This script prepares feedback for students by:
# 1. converting their Xournal++ files to PDFs
# 2. reading feedback for them (including grades) from an .ods file and saves that in a PDF file.
#
# Example usage: ./prepare_feedback.sh source_dir grades.ods target_dir
#
# We expect the source dir to be structured as follows:
# source_dir
# ├── exercise1
# │   ├── Student Name_1234_assignsubmission_file
# │   │   ├── file1.xopp
# │   │   └── file1.pdf
# │   └── Other Student Name_5678_assignsubmission_file
# │       ├── file1.xopp
# │       └── file1.pdf
# └── exercise2
#    ├── Student Name_9999_assignsubmission_file
#    │   ├── file1.xopp
#    │   └── file1.pdf
#    └── Other Student Name_7765_assignsubmission_file
#        ├── file1.xopp
#        └── file1.pdf
#
#
# The generated target directory will have this structure:
# target_dir
# ├── Student Name_9999_assignsubmission_file
# │   ├── Student Name.pdf
# │   ├── exercise1
# │   │   └── feedback.pdf
# │   └── exercise2
# │       └── feedback.pdf
# └── Other Student Name_7765_assignsubmission_file
#     ├── Other Student Name.pdf
#     ├── exercise1
#     │   └── feedback.pdf
#     └── exercise2
#         └── feedback.pdf
#
# Note that we use the ID of the last exercise
# "Student Name.pdf" is generated from the .ods file passed as an argument.

# Exit with an error when using an unbound variable
set -eu

create_pdf_with_grade_for_student() {
  # The input file is the CSV file containing a single student's feedback, including their grade
  csv_file_with_all_students="$1"

  student_name="$2"
  pdf_output="$3"

  # The CSV file with the individual student's feedback is in the same directory as the CSV file with all students
  student_csv="$(dirname "$csv_file_with_all_students")/$student_name.csv"
  # Write the column headers and the row for the student to the CSV file
  { head -n 1 "$csv_file_with_all_students"; grep -i "^$student_name," "$csv_file_with_all_students"; } > "$student_csv"

  # We'll create a Markdown file from the CSV as an intermediate step. We convert this Markdown file to a PDF
  md_output_file="${student_csv%.*}.md"

  awk --csv '
  NR == 1 {
      # Read headers into an array
      for (i = 1; i <= NF; i++) {
          headers[i] = $i;
      }
  }
  NR == 2 {
      # Map headers to values and output in Markdown list format
      for (i = 1; i <= NF; i++) {
          value = $i;

          # Remove surrounding quotes from the value
          gsub(/^"|"$/, "", value);

          # Print the header
          printf "- **%s**: ", headers[i];

          # Handle empty value
          if (value == "" || value == "\"\"") {
              print "";  # No value, just a newline
              continue;
          }

          # Check if the value contains commas
          if (value ~ /,/) {
              # Split the value into an array by commas
              n = split(value, subvalues, ",");
              print "";  # Start a new line for the sublist
              for (j = 1; j <= n; j++) {
                  # Trim whitespace from subvalues
                  subvalues[j] = gensub(/^[ \t]+|[ \t]+$/, "", "g", subvalues[j]);
                  printf "  - %s\n", subvalues[j];
              }
          } else {
              # Print the value directly if no commas
              print value;
          }
      }
  }
  ' "$student_csv" > "$md_output_file"

  # Convert the generated Markdown file to a PDF
  echo "Creating PDF file $pdf_output"
  pandoc "$md_output_file" -o "$pdf_output"
}
source_dir="$1"
ods_file_with_feedback="$2"
target_dir="$3"

# Used for storing the CSV file generated from the .ods file that contains
# the feedback for the students
temp_dir=$(mktemp -d)
libreoffice --convert-to csv "$ods_file_with_feedback" --outdir "$temp_dir"
csv_file="$temp_dir/$(basename "$ods_file_with_feedback" .ods).csv"

echo "Preparing feedback in $source_dir"
echo "Output will be saved in $target_dir"

# Get the last directory inside the source directory
last_exercise=$(ls -1 "$source_dir" | tail -n 1)
echo "Last directory: $last_exercise"

# Create each student feedback directory and build a map from student name to their feedback directory
declare -A student_feedback_dir_map
for student_dir in "$source_dir/$last_exercise"/*; do
    if [ -d "$student_dir" ]; then

        # We assume the student directory starts with the student name followed by an underscore
        student_name="$(basename "$student_dir" | grep -Eo '^[^_]+')"

        target_student_dir="$target_dir/$(basename "$student_dir")"
        student_feedback_dir_map["$student_name"]="$target_student_dir"
        mkdir -p "$target_student_dir"

    fi
done
# printf "%s\n" "${!student_feedback_dir_map[@]}" "${student_feedback_dir_map[@]}" | pr -2t -J
for x in "${!student_feedback_dir_map[@]}"; do printf "[%s]=%s\n" "$x" "${student_feedback_dir_map[$x]}" ; done


for task_dir in $source_dir/*; do
  echo "Processing exercise $task_dir"
  for student_dir in "$task_dir"/*; do
    if [ -d "$student_dir" ]; then

        # The student name is the key into the map, the value is the target directory
        student_name="$(basename "$student_dir" | grep -Eo '^[^_]+')"

        target_student_dir="${student_feedback_dir_map[$student_name]}"

        # create_pdf_with_grade "$temp_file_for_student_feedback" "$target_student_dir/$student_name.pdf"
        create_pdf_with_grade_for_student "$csv_file" "$student_name" "$target_student_dir/$student_name.pdf"

        target_student_dir_for_task="$target_student_dir/$(basename "$task_dir")"

        echo "Processing student $student_name in $student_dir. Saving feedback for task in $target_student_dir_for_task"
        mkdir -p "$target_student_dir_for_task"

        # Convert each .xopp file in the student's dir to a PDF and save it in the target directory
        for src_xopp in "$student_dir"/*.xopp; do
          if [ ! -f "$src_xopp" ]; then
            echo "No .xopp files found in $student_dir"
            # Copy the existing PDF file if it exists
            cp "$student_dir"/*.pdf "$target_student_dir_for_task"
          else
            target_pdf="$target_student_dir_for_task/$(basename "$src_xopp" .xopp).pdf"
            xournalpp --create-pdf="$target_pdf" "$src_xopp"
          fi
        done
    fi
  done
done


