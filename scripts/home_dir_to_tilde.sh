#!/bin/bash

# Turns a file path from "/home/mb/a_dir" to "~/a_dir" if the current user's home is "home/mb".

# Either read the file path from the first argument or from stdin if no argument is given
if [ -n "$1" ]; then
  file_path="$1"
else
  read file_path
fi

# If the path starts with the value of $HOME, we remove $HOME from the start
# From https://stackoverflow.com/questions/10036255/is-there-a-good-way-to-replace-home-directory-with-tilde-in-bash#10037257
[[ "$file_path" =~ ^"$HOME"(/|$) ]] && file_path="~${file_path#$HOME}"

printf "$file_path"
