# Creates .mp3 files from all .wav files in the current directory.
# Keeps the original .wav files.

wav_suffix=".wav"
for i in *$wav_suffix; do
    [ -f "$i" ] || break
    # Remove the .wav suffix from the file name
    base_name=${i%$wav_suffix}
    mp3_file_name="$base_name".mp3
    echo "Convert $i to $mp3_file_name"
    ffmpeg -i "$i" "$mp3_file_name"
done
