# Creates .ogg files from all .m4a files in the current directory.
# Keeps the original .m4a files.

m4a_suffix=".m4a"
for i in *$m4a_suffix; do
    [ -f "$i" ] || break
    # Remove the .m4a suffix from the file name
    base_name=${i%$m4a_suffix}
    ogg_file_name="$base_name".ogg
    echo "Convert $i to $ogg_file_name"
    ffmpeg -i "$i" "$ogg_file_name"
done
