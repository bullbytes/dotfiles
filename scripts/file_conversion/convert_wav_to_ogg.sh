#!/usr/bin/bash

# Creates .ogg files from all .wav files in the argument path (current directory if no argument was given).
# Keeps the original .wav files.

path="$1"

# Use the current directory if the user didn't pass an argument
if [ -z "$path" ]; then
  path="$(pwd)"
fi
wav_suffix=".wav"
for i in "${path}"/*$wav_suffix; do
    [ -f "$i" ] || break
    # Remove the .wav suffix from the file name
    base_name=${i%$wav_suffix}
    ogg_file_name="$base_name".ogg
    echo "Convert $i to $ogg_file_name"
    ffmpeg -hide_banner -i "$i" "$ogg_file_name"
done

