#!/usr/bin/bash

# Reencodes a video or audio file using the given constant rate factor (CRF).

input_file="$1"
target_dir="$2"
crf="$3"

echo "Input file: $input_file"

file_ext="${input_file##*.}"

base_name="${input_file%.*}"

output_file="$target_dir/${base_name}_crf_$crf.${file_ext}"

echo "Output file: $output_file"

# Use movflags and map_metadata to copy the input file's metadata to the output file
ffmpeg -i "$input_file" -movflags use_metadata_tags -map_metadata 0 -crf "$crf" "$output_file"
