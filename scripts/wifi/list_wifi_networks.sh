#!/usr/bin/env bash

interface_name="wlp3s0"

# See: https://askubuntu.com/questions/75625/how-do-i-scan-for-wireless-access-points#832592
ip link set $interface_name up
iw dev $interface_name scan | egrep "signal|SSID" | sed -e "s/\tsignal: //" -e "s/\tSSID: //" | awk '{ORS = (NR % 2 == 0)? "\n" : " "; print}' | sort

