#! /usr/bin/env bash

# Connects to a wireless network secured with WPA2

if (( $# >= 2 )); then
  ssid="$1"
  passphrase="$2"
  interface_name="wlp3s0"

  echo "Connecting to $ssid via interface $interface_name"

  # If we run this script multiple times without ending the processes before starting them, we'll end up with no Internet connection at all (`ip link show dev wlp3s0` will show NO-CARRIER)
  pkill wpa_supplicant
  pkill dhcpcd

  # wpa_passphrase creates a minimal configuration including a pre-shared key that we pass to wpa_supplicant.
  # -B causes wpa_supplicant to run in the background
  # We can't use process substitution a la "-c <(wpa_passphrase ...)" since sudo closes all file descriptors except for the standard ones (stdin, stdout, and stderr). See: https://unix.stackexchange.com/questions/279545/failed-to-open-config-file-dev-fd-63-error-no-such-file-or-directory-for-wp
  wpa_passphrase "$ssid" "$passphrase" | sudo wpa_supplicant -B -i "$interface_name" -c /dev/stdin

  # Request an IP address
  sudo dhcpcd $interface_name

  ## Log the IP address to a file
  logDir="/var/log/scripts"
  mkdir -p $logDir

  ipAddress=$(ip route get 8.8.8.8 | cut -d ' ' -f3)

  logFile="$logDir/connect_to_$ssid.log"
  echo "$ipAddress $(date)" >> "$logFile"
  echo "Logged IP address to $logFile"

  # To end the WiFi connection, use "wpa_cli -i wlp3s0 terminate"

  else
    echo "Usage: connect_to_network.sh ssid password"
fi

