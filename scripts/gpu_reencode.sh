#!/usr/bin/bash

# Reencodes a video or audio file using a graphics card.

input_file="$1"
target_dir="$2"
q="$3"

echo "Input file: $input_file"

file_ext="${input_file##*.}"

base_name="${input_file%.*}"

output_file="$target_dir/${base_name}_av1_q_$q.${file_ext}"

echo "Output file: $output_file"

# Use movflags and map_metadata to copy the input file's metadata to the output file
# See also https://askubuntu.com/questions/1107782/how-to-use-gpu-acceleration-in-ffmpeg-with-amd-radeon/1520791#1520791
ffmpeg -hwaccel vaapi -hwaccel_output_format vaapi -i "$input_file" -movflags use_metadata_tags -map_metadata 0 -vf 'format=vaapi,hwupload' -c:v av1_vaapi -q "$q" "$output_file"

