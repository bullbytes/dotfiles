# Adapted from https://stackoverflow.com/questions/30577923/how-to-show-all-colors-supported-by-bash#30578079

for i in $(seq 0 15); do
    tput setaf $i
    echo "This is color $i"
done
