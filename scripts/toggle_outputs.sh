outputs=$(swaymsg -t get_outputs | jq -r '.[] | [.name, .make, .active] | @tsv' | sed 's/\t/ /g; s/true$/✅/; s/false$/❌/')
line_of_selected_output=$(fuzzel --dmenu --prompt "🖥️" --width 80 <<< "$outputs")
# The output's name (e.g., "LVDS-1") is the first field
name_of_selected_output=$(cut -f1 -d' ' <<< "$line_of_selected_output")
swaymsg output "$name_of_selected_output" toggle
