#!/usr/bin/env python

# Deduplicates the lines of a file.
# Doesn't change the file, just writes the result
# to standard out.

import sys

if len(sys.argv) >= 2:
    unique_lines = []

    file_name = sys.argv[1]

    with open(file_name, 'r') as fi:

        # If a command occurs multiple times in the file, we keep the more recent
        # one (the one closer to the end of .bash_history).
        # This is useful when searching .bash_history with FZF using ctrl+r, since
        # per default, FZF reverses the lines in .bash_history and we see more recent
        # commands first.
        for line in reversed(list(fi)):
            if line not in unique_lines:
                unique_lines.insert(0, line)

    for unique_line in unique_lines:
        print(unique_line, end='')

else:
    print('Please provide an input file path', file=sys.stderr)

