# See https://unix.stackexchange.com/questions/103252/how-to-retrieve-a-webpages-title-using-the-command-line

url="$1"

# 1. curl downloads the HTML
# 2. grep extracts the web page title, stops reading after the first matching line with -m1
# 3. perl converts HTML entities to plain text, for example: &amp; -> &
#
# w3m can also convert HTML entities but creates line breaks if the page title is lone: "| w3m -dump -T text/html"
# Maybe once this is fixed we could use w3m: https://github.com/tats/w3m/issues/286

curl -sL "$url" | grep -ioP -m1 '<title[^>]*>\s*\K[^<]*' | perl -MHTML::Entities -pe 'decode_entities($_);'
