# Utility functions for Bash scripts

# Removes leading and trailing whitespace from the input parameter
trim() {
    local var="$*"
    # remove leading whitespace characters
    var="${var#"${var%%[![:space:]]*}"}"
    # remove trailing whitespace characters
    var="${var%"${var##*[![:space:]]}"}"
    printf '%s' "$var"
}


stringContain() { case $2 in *$1* ) return 0;; *) return 1;; esac ;}
