#!/bin/bash

function log () {
  printf "$1" | systemd-cat -t screenshotScript
}

function showHelp()
{
  echo "Takes a screenshot using grim"
  echo
  echo "Syntax: take_screenshot [-e|h]"
  echo "options:"
  echo "e     Set extent of screenshot. Valid arguments: current_screen, all_screens, rectangle"
  echo "h     Print this help."
  echo
  echo "Example: make_screenshot.sh -e all_screens"
}

screenshots_dir="${GRIM_DEFAULT_DIR:-~}"

# Create the directory for the screenshots if it doesn't exist
mkdir -p "$screenshots_dir"

screenshot_path="$screenshots_dir/screen_$(date "+%F_%H-%M-%S").png"


screenshot_extent="current_screen"

while getopts ":he:" option; do
  case $option in
    h)
      showHelp
      exit;;
    e)
      screenshot_extent=$OPTARG;;
    *)
      echo "Error: Invalid option"
      showHelp
      exit;;
  esac
done

took_screenshot=false

case $screenshot_extent in
  all_screens)
    grim "$screenshot_path"
    echo "Took screenshot of all screens"
    took_screenshot=true
    ;;
  current_screen)
    # The currently focused screen, for example: LVDS-1
    # From here: https://gist.github.com/Mel34/ab9b6d562f9181ed8bbdc7c76022b85b
    cur_screen="$(swaymsg -t get_outputs | jq -r '.[] | select(.focused) | .name')"
    grim -o "$cur_screen" "$screenshot_path"
    echo "Took screenshot of $cur_screen"
    took_screenshot=true
    ;;
  rectangle)
    # -d shows the dimensions of the rectangle while drawing it
    grim -g "$(slurp -d)" "$screenshot_path"
    took_screenshot=true
    ;;
  *)
    echo "Error: Invalid option for screenshot extent: $screenshot_extent"
esac

# Have a symbolic link to the latest screenshot. In Sway's config, there's a keybinding to open the latest screenshot in an annotation tool

if [ "$took_screenshot" = true ]; then
  echo "Screenshot file path: $screenshot_path"
  ln --symbolic --force "$screenshot_path" "$screenshots_dir/latest_screenshot.png"
fi
