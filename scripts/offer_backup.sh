#!/bin/bash

# This script asks the user to do a backup once both the source partition and the destination partition are mounted.
# Rsync copies the data from the source partition to the destination partition.
# Mind that this relies on kitty, Sway, and Bash being present.

# After boot, the directory /run/media/ exists but /run/media/$USER/ does not. inotifywait can't watch directories that don't exist which is why we watch /run/media/ recursively.
mount_dir="/run/media/"
user_mount_dir=$mount_dir$USER
src_partition="$1"
dest_partition="$2"

src_and_dest_are_mounted () {
  [ -d "$user_mount_dir/$src_partition" ] && [ -d "$user_mount_dir/$dest_partition" ]
}

back_up_data_from_src_to_dest () {

  echo About to start kitty with the backup dialog

  # Open a floating centered Sway window with Kitty as the terminal emulator inside
  kitty bash -c "swaymsg floating enable; swaymsg move position center; swaymsg resize set 80ppt 80ppt;\
                 read -p \"Use rsync to back up data from $user_mount_dir/$src_partition to $user_mount_dir/$dest_partition? Type 'yes' to confirm: \" user_input;\
                 [ \"\$user_input\" = \"yes\" ] &&\
                 (echo User confirmed backup; rsync --exclude=.vifm-Trash-* --exclude=.Trash-* --hard-links --archive -v --progress --delete $user_mount_dir/$src_partition $user_mount_dir/$dest_partition/)\
                 || echo You cancelled the backup; read -p \"End of backup\""
}

# Check if the partitions are already mounted when this script starts
# src_and_dest_are_mounted && back_up_data_from_src_to_dest

echo "Watching if both $src_partition and $dest_partition appear in $user_mount_dir"
echo "Display variable is: $DISPLAY"

# If either the source or the destination partition is mounted which leads to both of them being mounted, we ask the user to back up the data
inotifywait --monitor --recursive "$mount_dir" -e create |
    while read path action file; do
        echo "The file '$file' appeared in directory '$path' via '$action'"

        # Without this check we would ask for a backup each time a new unrelated partition is mounted while source and destination partitions exist
        [ "$file" = "$src_partition" -o "$file" = "$dest_partition" ] &&
          src_and_dest_are_mounted &&
          back_up_data_from_src_to_dest
    done

echo Backup script is done
