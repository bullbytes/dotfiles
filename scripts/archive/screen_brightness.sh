#!/usr/bin/bash

# Changes the screen brightness.
# Dependencies: bc

# To allow users of group "video" to change the brightness, do this:
# bash -c 'cat add_this_to_backlight_rules >> /etc/udev/rules.d/backlight.rules'
# user="mb"
# echo "Adding user ${user} to group video to allow changing the monitor backlight"
# gpasswd --add ${user} video


# Alternatives:
# https://github.com/Hummer12007/brightnessctl
# https://gitlab.com/wavexx/acpilight
# https://github.com/haikarainen/light

# Usage examples: "screen_brightness.sh +20%", "screen_brightness.sh -500", "screen_brightness 3000"

# If the parameter is a string like "20%" this functions calculates
# 20 percent of the maximum brightness. If the parameter is a string
# without a trailing percentage sign, it just returns the parameter.
function get_absolute_change {
  # This can be the change with percentage sign "20%" or without "20"
  change=$1
  change_without_percentage_sign=${change%"%"}
  if [[ "$change" == "$change_without_percentage_sign" ]]; then
    # There was no trailing percentage sign
    absolute_change=$change
  else
    max_brightness=$(cat $max_brightness_file)
    # Get the value for changing the brightness one percent
    absolute_change=$(echo "($max_brightness / 100) * $change_without_percentage_sign" | bc -l)
    # Remove digits after the decimal point to make it an integer
    absolute_change=$(printf %.0f $absolute_change)
  fi

  echo $absolute_change
}

if (( $# == 1 )); then
  argument="$1"

  # Writing to this file changes the screen brightness.
  # The parentheses are used to have a wildcard for the
  # directory containing the brightness file. The directory
  # could be "intel_backlight" or "acpi_video0", for example
  brightness_dir=(/sys/class/backlight/*)
  brightness_file=${brightness_dir}/brightness
  max_brightness_file=${brightness_dir}/max_brightness

  current_brightness=$(cat "${brightness_file}")

  if [[ $argument = +* ]]; then
    # Remove the leading +
    change=${argument#*+}

    absolute_change=$(get_absolute_change $change)
    new_brightness=$((current_brightness + absolute_change))
  elif [[ $argument = -* ]]; then
    # Remove the leading -
    change=${argument#*-}

    absolute_change=$(get_absolute_change $change)
    new_brightness=$((current_brightness - absolute_change))
  else
    new_brightness="$argument"
  fi

  # If the argument is something else than an integer, this will
  # yield "write error: Invalid argument"
  echo "${new_brightness}" > "${brightness_file}"

else
  echo "Please pass a single argument. Usage examples: \"screen_brightness.sh +20%\", \"screen_brightness.sh -500\", \"screen_brightness 3000\""
fi
