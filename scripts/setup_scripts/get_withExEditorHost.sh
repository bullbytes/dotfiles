#!/usr/bin/env bash

# Make the script fail immediately when a command fails and don't allow unset variables
set -euo pipefail

function main {
  # Install or update the host for withExEditor that allows editing text in the browser using an editor like Vim.
  # After installing for the first time, run "~/.local/bin/withExEditorHost/index setup"
  # After executing this script, reload the browser plugin

  # See https://github.com/asamuzaK/withExEditorHost/releases for supported operating systems
  os="linux-x86_64"

  # Possible values: firefox, waterfoxcurrent, chrome, chromebeta, chromium, brave, vivaldi
  browser="firefox"

  # Allowed tags: "latest" and "next" (pre-release)
  versionTag="latest"
  # The host's version number
  version=$(curl --silent https://registry.npmjs.org/withexeditorhost | jq --raw-output ".\"dist-tags\".\"${versionTag}\"")

  withExEditorHostRemoteFile="https://github.com/asamuzaK/withExEditorHost/releases/download/v${version}/${os}.tar.gz"
  withExEditorHostLocalArchive="/tmp/withExEditorHost.tar.gz"
  withExEditorHostDir="${HOME}/.local/bin/withExEditorHost"

  echo "Downloading withExEditorHost ${version} for ${browser}"

  # Create the dir for the host's index file, download the archive and extract it
  mkdir --parents "${withExEditorHostDir}"
  # If the URL returns 404, make cURL fail. This prevents the archive extractor from trying to extract an HTML error page
  if curl --fail -L -o "${withExEditorHostLocalArchive}" "${withExEditorHostRemoteFile}"\
  && extractTarGz "${withExEditorHostLocalArchive}" "${withExEditorHostDir}"; then

    indexFile="${withExEditorHostDir}/index"
    withExEditorHostConfigDir="${HOME}/.config/withexeditorhost/config/${browser}"
    mkdir --parents "${withExEditorHostConfigDir}"

    hostScript="${withExEditorHostConfigDir}/withexeditorhost.sh"
    echo "Saving host script at ${hostScript}"
    # The browser plugin will use this shell script to call the host's index file
    printf "#!/usr/bin/env bash\n%s\n" "${indexFile}" > "${hostScript}"

    chmod +x "${indexFile}" "${hostScript}"
  fi
}

# Extracts the files of a tar.gz archive (first parameter) to a destination directory (second parameter).
# Uses either bsdtar (from libarchive), tar, or 7z.
function extractTarGz {

  if hash bsdtar 2>/dev/null; then
    echo "Extracting with bsdtar to $2"
    bsdtar -xf "$1" --directory "$2"

  elif hash tar 2>/dev/null; then
    echo "Extracting with tar to $2"
    tar -zxf "$1" --directory "$2"

  elif hash 7z 2>/dev/null; then
    echo "Extracting with 7z to $2"
    7z x "$1" -o"$2"

  else
    echo "No program found to extract a tar.gz archive. Please install bsdtar, tar, or 7z."
  fi
}
main
