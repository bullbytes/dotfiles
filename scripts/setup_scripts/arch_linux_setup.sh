# Purpose
# This script configures my Arch Linux semi-automatically
# It's expected to be run as root.

# Inspiration
# https://github.com/purpleKarrot/dotfiles/blob/86da60ba8a16f80209a775c6bc131c8c65a83786/install.sh

# Install packages by reading package names from a file
arch_packages="./arch_packages"
echo "Installing packages from $arch_packages"
# Remove lines that start with optional whitespace followed by a pound sign
# Replace newlines with spaces and remove the superfluous spaces generated that way
packages=$(sed 's/^[[:space:]]*#.*//' "$arch_packages" | tr '\n' ' ' | sed -E 's/  +/ /g;s/^ //')
pacman -Sy $packages

timedatectl set-timezone Europe/Vienna

# What's left to do manually:
# * Make symbolic (or hard) links from ~/ to files like .bashrc, .bash_profile, .inputrc.
# Copy custom systemd services and timers to ~/.config/systemd/user/ and enable them.
# Set up Firefox with its addons.
